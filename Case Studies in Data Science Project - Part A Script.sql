use DS
go
----------------------------------------------DB Creation---------------------------------

DROP TABLE dbo.Drivers 
DROP TABLE dbo.Accidents
DROP TABLE dbo.Parents
DROP TABLE dbo.Convictions

create table Drivers (
	Id1 bigint,
	min int,
	taarich_rishyon int,
	gil int,
	KvutzaUchlusiya int,
	Yabeshetint int,
	Aliya int,
	ofnoa_ind int,
	hagbala1 int,
	hagbala2 int,
	hagbala3 int,
	hagbala4 int,
	hagbala5 int,
	v_oy int,
	mahoznafa int,
	shiuch int,
	Zturat_yeshuv int,
	St_indication_or int,
	Id2 int,
	Id2version int,
	Id3 int,
	Id3version int)
				

CREATE TABLE Parents (
	id bigint,
	idversion int,
	Kvutza int,
	Yabeshet int,
	Aliya int,
	Matzav int,
	Educg int,
	Murshe int,
	Cars int,
	rf1 int,
	rf2 int,
	rf3 int,
	SHNAT_YITZUR1 int,
	SHNAT_YITZUR2 int,
	SHNAT_YITZUR3 int,
	Nefah_1 int,
	Nefah_2 int,
	Nefah_3 int)
	

CREATE	TABLE	Accidents	(
	Id1	bigint,		
	Id2	bigint,		
	Id3	bigint,		
	PK_TEUNA_FIKT	SMALLINT,		
	rehevfict	SMALLINT,		
	SUG_MEORAV	SMALLINT,		
	HUMRAT_PGIA	SMALLINT,		
	SUG_REHEV_NASA	SMALLINT,		
	EMZAE_BETIHUT	SMALLINT,		
	SHAA	SMALLINT,		
	SUG_YOM	SMALLINT,		
	YOM_LAYLA	SMALLINT,		
	YOM_BASHAVUA	SMALLINT,		
	RAMZOR	SMALLINT,		
	SUG_DEREH	SMALLINT,		
	MEHIRUT_MUTERET	SMALLINT,		
	HUMRAT_TEUNA	SMALLINT,		
	SUG_TIK	SMALLINT,		
	GOREM_TEUNA	SMALLINT,		
	SUG_TEUNA	SMALLINT,		
	HAD_MASLUL	SMALLINT,		
	RAV_MASLUL	SMALLINT,		
	ZURAT_DEREH	SMALLINT,		
	TKINUT	SMALLINT,		
	ROHAV	SMALLINT,		
	SIMUN_TIMRUR	SMALLINT,		
	TEURA	SMALLINT,		
	BAKARA	SMALLINT,		
	MEZEG_AVIR	SMALLINT,		
	PNE_KVISH	SMALLINT,		
	nezek	SMALLINT,		
	nefah	SMALLINT,		
	shnat_yitzur	SMALLINT,		
	driver	SMALLINT,		
	Vetek_prati	SMALLINT,		
	Vetek_ofnoa	SMALLINT,		
	mahoznafa	SMALLINT,		
	shiuch	SMALLINT,		
	zturatYishuv	SMALLINT,		
	merhak	SMALLINT)			


CREATE TABLE Convictions (
	Id1 bigint,
	Id2 bigint,
	Id3 bigint,
	Kod int,
	KodKvutzaLMS_1 int,
	KodKvutzaLMS_2 int,
	KodKvutzaLMS_3 int,
	StatusHarshaa int,
	statusteuna int,
	avera int,
	vetek_a int)


------------------------------------------ETL Variable transformation------------------------

ALTER TABLE dbo.Drivers DROP COLUMN hasGlasses 
ALTER TABLE dbo.Drivers DROP COLUMN isCripple 
ALTER TABLE dbo.Drivers DROP COLUMN medicalIssue 
ALTER TABLE dbo.Drivers DROP COLUMN otherDisabilities 
go

ALTER TABLE dbo.Drivers ADD hasGlasses SMALLINT
ALTER TABLE dbo.Drivers ADD isCrippele SMALLINT
ALTER TABLE dbo.Drivers ADD medicalIssue SMALLINT
ALTER TABLE dbo.Drivers ADD otherDisabilities SMALLINT
go

UPDATE dbo.Drivers
SET hasGlasses =
				(CASE
	WHEN (hagbala1= 1) OR (hagbala2= 1) OR (hagbala3= 1) OR (hagbala4= 1) OR (hagbala5= 1) THEN 1    
	ELSE  -1      /* 1=HasGlasses, -1=Doesn't Have Glasses*/
END)

UPDATE dbo.Drivers
SET isCrippele =
				(CASE
	 WHEN (hagbala1= 2) OR (hagbala2= 2) OR (hagbala3= 2) OR (hagbala4= 2) OR (hagbala5= 2) THEN 1    
	ELSE  -1      /* 1=Is Crippele, -1=Isn't Crippele*/
END)

UPDATE dbo.Drivers
SET medicalIssue =
				(CASE
	 WHEN (hagbala1= 3) OR (hagbala2= 3) OR (hagbala3= 3) OR (hagbala4= 3) OR (hagbala5= 3) THEN 1    
	 ELSE  -1      /* 1=has medical issue, -1=Healthy*/
END)


ALTER TABLE dbo.Drivers ADD  otherDisabilities SMALLINT

UPDATE dbo.Drivers 
SET otherDisabilities =k1.CountOthersDisabilties
	FROM 
	/*CountOthersDisabilties Feature extraction of- count number of 4 value appearence */
(SELECT * 	
FROM
   dbo.Drivers
   CROSS APPLY (
      SELECT Count(*)
      FROM (VALUES (hagbala1),(hagbala2),(hagbala3),(hagbala4),(hagbala5)) C (Val)
      WHERE Val = 4
   ) A (CountOthersDisabilties))  AS k1
   WHERE k1.id1= dbo.Drivers.id1  /* 2=has two other disabilties, 1=has one other disabilties, -1=Without other disabilities */


   /*Merging Or Yarok kit time frame categories on class 6,7,8   */
   UPDATE dbo.Drivers
SET v_oy =
				(CASE
	 WHEN (v_oy=7) OR (v_oy=8) THEN 6
	 WHEN (v_oy=9) THEN 7
END)

   /*Change category names on Matzav*/
   UPDATE dbo.Parents
SET Matzav =
				(CASE
	 WHEN (Matzav=10)THEN 1
	 WHEN (Matzav=20)THEN 2
	 WHEN (Matzav=30)THEN 3
	 WHEN (Matzav=40)THEN 4
	 END)


			 /*                                                        K2 Accidents                                                  */				/*Updating Parents id in Accidents*/
	UPDATE dbo.Accidents
SET Accidents.id2 =Drivers.id2
	FROM dbo.Drivers
	WHERE ((dbo.Drivers.id1= dbo.Accidents.id1)   AND (dbo.Accidents.Id2 IS NULL))   

	UPDATE dbo.Accidents
SET Accidents.Id3 =Drivers.Id3	
	FROM dbo.Drivers
	WHERE ((dbo.Drivers.id1= dbo.Accidents.id1)   AND (dbo.Accidents.Id3 IS NULL))  

   /*Merging shnat_yitzur time frame categories, grouping all the years that are before 1980 */
   UPDATE dbo.Accidents
SET shnat_yitzur =
				(CASE
	 WHEN (shnat_yitzur<=980)THEN 1980
	 WHEN (shnat_yitzur<1980) THEN shnat_yitzur
	 END)


--------------------------------------------------------Creat Target Variables---------------------------------------------------


SELECT * 
INTO youngDrivers
FROM dbo.Accidents 

drop table dbo.youngDrivers

ALTER TABLE dbo.youngDrivers ADD AccidentsAmount SMALLINT
ALTER TABLE dbo.youngDrivers ADD InvolvedInAccident SMALLINT
ALTER TABLE dbo.youngDrivers ADD InvolvedInLethalAccident SMALLINT


-------------------------------------------------------------------Adding accidents amount per driver--------------------------------------
update dbo.youngDrivers 
SET  AccidentsAmount= (SELECT COUNT(*) AS AccidentsAmount1 
FROM dbo.youngDrivers as yd
WHERE driver=1 AND dbo.youngDrivers.id1=yd.id1
) 

-------------------------------------------------------------Target Variable for Predicting if involved in accident -----------------------------------------
update dbo.youngDrivers 
SET  InvolvedInAccident= (CASE WHEN AccidentsAmount>0 THEN 1 ELSE 0 END)

-------------------------------------------------------------Target Variable for Predicting if Involved In Lethal Accedent -----------------------------------------

update dbo.youngDrivers 
SET  InvolvedInLethalAccident= (CASE WHEN (dbo.youngDrivers.HUMRAT_TEUNA = 1 AND dbo.youngDrivers.driver=1) THEN 1 ELSE 0 END)


---------------------------------------
SELECT * 
INTO ConvictionSummary
FROM dbo.Convictions

--drop table dbo.youngDrivers

ALTER TABLE dbo.ConvictionSummary ADD ReportsAmount SMALLINT
ALTER TABLE dbo.ConvictionSummary ADD ConvictionsAmount SMALLINT
ALTER TABLE dbo.ConvictionSummary ADD violationsCounter INT



UPDATE ConvictionSummary
SET violationsCounter =
      (CASE
            WHEN KodKvutzaLMS_1='' AND KodKvutzaLMS_2='' AND KodKvutzaLMS_3='' THEN 0     
            WHEN KodKvutzaLMS_1=40 AND KodKvutzaLMS_2=40 AND KodKvutzaLMS_3=40 THEN 1
            WHEN (KodKvutzaLMS_1=40 AND KodKvutzaLMS_2=40 AND (KodKvutzaLMS_3!=40 AND KodKvutzaLMS_3!='')) OR
                  (KodKvutzaLMS_1=40 AND (KodKvutzaLMS_2!=40 AND KodKvutzaLMS_2!='') AND KodKvutzaLMS_3=40) OR
                  ((KodKvutzaLMS_1!=40 AND KodKvutzaLMS_1!='') AND KodKvutzaLMS_2=40 AND KodKvutzaLMS_3=40) THEN 1
            WHEN (KodKvutzaLMS_1=40 AND (KodKvutzaLMS_2!=40 AND KodKvutzaLMS_2!='') AND (KodKvutzaLMS_3!=40 AND KodKvutzaLMS_3!='')) OR
                  ((KodKvutzaLMS_1!=40 AND KodKvutzaLMS_1!='') AND (KodKvutzaLMS_2!=40 AND KodKvutzaLMS_2!='') AND KodKvutzaLMS_3=40) OR
                  ((KodKvutzaLMS_1!=40 AND KodKvutzaLMS_1!='') AND KodKvutzaLMS_2=40 AND (KodKvutzaLMS_3!=40 AND KodKvutzaLMS_3!='')) THEN 2         
            WHEN (KodKvutzaLMS_1!=40 AND KodKvutzaLMS_2!=40 AND KodKvutzaLMS_3!=40) THEN 3
END)


-------Creating a table in order to calculate the Young Drivers Convictions --------------

SELECT DISTINCT id1, id2, id3, avera AS youngDriverFelony, sum (violationsCounter) AS sumYoungDriverFelonies
INTO driverConvictions
FROM ConvictionSummary
WHERE avera=1 AND StatusHarshaa <> 3
GROUP BY id1, id2, id3, avera	
	
-------Creating a table in order to calculate the Dad's Convictions --------------

SELECT DISTINCT id1, id2, id3, avera AS dadFelony, sum (violationsCounter) AS sumDadFelonies
INTO dadConvictions
FROM ConvictionSummary
WHERE avera=2 AND StatusHarshaa <> 3
GROUP BY id1, id2, id3, avera	

-------Creating a table in order to calculate the Mom's Convictions --------------

SELECT DISTINCT id1, id2, id3, avera AS momFelony, sum (violationsCounter) AS sumMomFelonies
INTO momConvictions
FROM ConvictionSummary
WHERE avera=3 AND StatusHarshaa<> 3
GROUP BY id1, id2, id3, avera	


ALTER TABLE dbo.youngDrivers ADD AccidentsAmount SMALLINT
ALTER TABLE dbo.youngDrivers ADD InvolvedInAccedent SMALLINT
ALTER TABLE dbo.youngDrivers ADD InvolvedInLethalAccedent SMALLINT

SELECT DISTINCT dr.Id1 , yd.AccidentsAmount, yd.InvolvedInAccident, yd.InvolvedInLethalAccident
FROM Drivers AS dr join youngDrivers AS yd ON dr.Id1=yd.Id1 join ConvictionSummary AS cs ON (dr.Id1=cs.Id1 AND dr.Id2=cs.Id2 AND dr.Id3=cs.Id3)

--Merging ID's with Target Values
SELECT DISTINCT dr.Id1 as yid, dr.Id2 as did,dr.Id3 as mid, yd.AccidentsAmount, yd.InvolvedInAccident, yd.InvolvedInLethalAccident
INTO Step1
FROM Drivers AS dr left outer join youngDrivers AS yd ON dr.Id1=yd.Id1 and dr.Id2=yd.Id2 and dr.Id3=yd.Id3

--Merging ID's & Target Values With Felonies Details

SELECT DISTINCT s1.yid,s1.did,s1.mid, s1.AccidentsAmount, s1.InvolvedInAccident, s1.InvolvedInLethalAccident, sumYoungDriverFelonies
INTO Step2
FROM (Step1 AS s1 left outer join driverConvictions AS dc ON dc.Id1=s1.yid) 

SELECT DISTINCT s2.yid,s2.did,s2.mid, s2.AccidentsAmount, s2.InvolvedInAccident, s2.InvolvedInLethalAccident, s2.sumYoungDriverFelonies,sumDadFelonies
INTO Step3
FROM (Step2 AS s2 left outer join dadConvictions AS dc ON dc.Id2=s2.did) 

SELECT DISTINCT s3.yid,s3.did,s3.mid, s3.AccidentsAmount, s3.InvolvedInAccident, s3.InvolvedInLethalAccident, s3.sumYoungDriverFelonies,s3.sumDadFelonies, mc.sumMomFelonies
INTO Step4
FROM (Step3 AS s3 left outer join momConvictions AS mc ON mc.Id3=s3.mid) 

--Merging ID's, Target Values & Felonies Details with Drivers Details

SELECT DISTINCT *
INTO Step5
FROM (Step4 AS s4 left outer join Drivers as D ON D.Id1=s4.yid and D.Id2=s4.did and D.Id3=s4.mid)

----- Merging Parents Details
Select id as MomID, idversion as MomIdVersion,Kvutza as MomKvotza,Aliya as MomAliya,Matzav as MomMatzav,Educg as MomEDU,Murshe as MomMurshe
Into Moms
From Parents

Select id as DadID, idversion as DadIdVersion,Kvutza as DadKvotza,Aliya as DadAliya,Matzav as DadMatzav,Educg as DadEDU,Murshe as DadMurshe
Into Dads
From Parents

SELECT DISTINCT *
INTO SummaryTable
FROM (Step5 AS s5 left outer join Dads as da ON da.DadID=s5.did) left outer join Moms as m ON m.MomID=s5.mid



ALTER TABLE dbo.Drivers DROP COLUMN hagbala1
ALTER TABLE dbo.Drivers DROP COLUMN hagbala2
ALTER TABLE dbo.Drivers DROP COLUMN hagbala3
ALTER TABLE dbo.Drivers DROP COLUMN hagbala4
ALTER TABLE dbo.Drivers DROP COLUMN hagbala5



SELECT * FROM dbo.Drivers
SELECT * FROM dbo.Parents
SELECT * FROM dbo.Accidents
SELECT * FROM dbo.Convictions
SELECT * FROM dbo.ConvictionSummary
SELECT * FROM dbo.Step2

